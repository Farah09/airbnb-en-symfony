<?php

namespace App\DataFixtures;

use App\Entity\Ad;
use Faker\Factory;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr-FR');
        $slugify = new Slugify();

        for($i = 1; $i <= 30; $i++){
            $ad = new Ad;

            $title = $faker->sentence(); //sentence met en paramètre le nbre de mot par defaut 6 donc là j'en ai 6
            $slug = $slugify->slugify($title);
            $coverImage = $faker->imageUrl(1000,350);
            $introduction = $faker->paragraph(2); // en parametres le nbre de phrase
            $content = '<p>' . join('<p></p>', $faker->paragraphs(5)) . '</p>'; // on a formaté 5 paragraphes en html

            $ad->setTitle($title)
                ->setSlug($slug)
                ->setCoverImage($coverImage)
                ->setIntroduction($introduction)
                ->setContent($content)
                ->setPrice(mt_rand(40, 200))
                ->setRooms(mt_rand(1, 5));

            // on va demander au manager de faire persister dans le temps ces infos
            $manager->persist($ad);
        }
        // flush ne rentre pas dans la boucle car il va envoyer une fois les 30 entrées d'un coup, pas besoin de se connecter 30 fois
        $manager->flush();
    }
}
